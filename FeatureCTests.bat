echo Running the build script. > FeatureCTest_Output.txt
call build.bat silent >> FeatureCTest_Output.txt

echo . >> FeatureCTest_Output.txt
echo Test one: Compiler does not allow comparison of booleans. >> FeatureCTest_Output.txt
echo ---- Expected Result: Compilation failure >> FeatureCTest_Output.txt
echo ---- Actual Result: >> FeatureCTest_Output.txt

echo on
call babycino.bat progs\features\TestBugC1.java progs\features\testBugC1.c >> FeatureCTest_Output.txt
echo off

echo . >> FeatureCTest_Output.txt
echo Compiling remaining Feature C tests with babycino. >> FeatureCTest_Output.txt
call babycino.bat progs\features\TestBugC2.java progs\features\testBugC2.c >> FeatureCTest_Output.txt
call babycino.bat progs\features\TestBugC2.java progs\features\testBugC3.c >> FeatureCTest_Output.txt

echo . >> FeatureCTest_Output.txt
echo Compiling resulting source code files with tcc. >> FeatureCTest_Output.txt
call tcc.bat -o progs\features\testBugC2.exe progs\features\testBugC2.c >> FeatureCTest_Output.txt
call tcc.bat -o progs\features\testBugC3.exe progs\features\testBugC3.c >> FeatureCTest_Output.txt

echo . >> FeatureCTest_Output.txt
echo Running tests... >> FeatureCTest_Output.txt

echo . >> FeatureCTest_Output.txt
echo Test two: Program evaluates equality incorrectly >> FeatureCTest_Output.txt
echo ---- Expected Result: Program should return true (1) when comparing equal values >> FeatureCTest_Output.txt
echo ---- Actual Result: >> FeatureCTest_Output.txt
progs\features\testBugC2.exe >> FeatureCTest_Output.txt

echo . >> FeatureCTest_Output.txt
echo Test three: Operator has a higher precedence than logical AND >> FeatureCTest_Output.txt
echo ---- Expected Result: Program should compile, and return true (1) >> FeatureCTest_Output.txt
echo ----  Actual Result: >> FeatureCTest_Output.txt
progs\features\testBugC3.exe >> FeatureCTest_Output.txt



