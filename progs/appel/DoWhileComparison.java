class DoWhileComparision {
    public static void main(String[] args){
		System.out.println(new DWC().Start(10));
	}
}

class DWC {
	int[] numbers;

    public int Start(int sz) {
		int aux01 ;
		aux01 = this.Init(sz);
		aux01 = this.Execute(sz);
		return 0;
    }

 	public int Execute(int size) {
		int i;
		int a;
		int b;
		boolean res;
		
		i = 0;
		
		do {
			System.out.println(1234);
			System.out.println(i);
			
			a = numbers[i];
			b = numbers[i+1];
			
			System.out.println(a);
			System.out.println(b);
			
			if (a <= b) {
				System.out.println(1);
			} else {
				System.out.println(0);
			}
			
			i = i + 1;
			
		} 
		while ( i <= ( size-1 ) );
		
		return 0;
		
    }
	
	public int Init(int size) {
		numbers = new int[size];
		
		numbers[0] = 20;
		numbers[1] =  7; 
		numbers[2] = 12;
		numbers[3] = 18;
		numbers[4] =  2; 
		numbers[5] = 11;
		numbers[6] =  9; 
		numbers[7] =  9; 
		numbers[8] = 19; 
		numbers[9] =  5;
		
		return 0;
	}
    
}