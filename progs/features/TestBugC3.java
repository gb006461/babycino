class Main {
    public static void main(String[] args){
		System.out.println(new TestBugC3().Start());
	}
}


class TestBugC3 {
	public int Start() {
		int a;
		int b;
		int c;
		
		a = 5;
		b = 5;
		c = 4;
	
		if (c <= a && a <= b) {
			System.out.println(1);
		} else {
			System.out.println(0);
		}
		
		return 0;
	}
}
