#include <stdio.h>
#include <stdlib.h>

union ilword {
    int n;
    union ilword* ptr;
    void(*f)();
};
typedef union ilword word;

word param[4];
int next_param = 0;

word r0 = {0};

word vg0 = {0};
word vg1 = {0};
word vg2 = {0};
void INIT();
void MAIN();
void DWC_Start();
void DWC_Execute();
void DWC_Init();
int main() {
    INIT();
    MAIN();
    return 0;
}

void INIT() {
    word vl[0];
    word r4 = {0};
    word r3 = {0};
    word r2 = {0};
    word r1 = {0};
    int p;
    for(p = 0; p <= -1 && p < 4; p++) {
        vl[p] = param[p];
    }
    next_param = 0;
INIT:
    r1.n = 1;
    r2.n = 0;
    vg0.ptr = calloc(r2.n, sizeof(word));
    r2.n = 0;
    vg1.ptr = calloc(r2.n, sizeof(word));
    r2.n = 3;
    vg2.ptr = calloc(r2.n, sizeof(word));
    r3 = vg2;
    r4.f = &DWC_Start;
    *(r3.ptr) = r4;
    r3.ptr = r3.ptr + r1.n;
    r4.f = &DWC_Execute;
    *(r3.ptr) = r4;
    r3.ptr = r3.ptr + r1.n;
    r4.f = &DWC_Init;
    *(r3.ptr) = r4;
    return;
}

void MAIN() {
    word vl[0];
    word r8 = {0};
    word r7 = {0};
    word r6 = {0};
    word r5 = {0};
    word r4 = {0};
    word r3 = {0};
    word r2 = {0};
    word r1 = {0};
    int p;
    for(p = 0; p <= -1 && p < 4; p++) {
        vl[p] = param[p];
    }
    next_param = 0;
MAIN:
    r1.n = 2;
    r2.ptr = calloc(r1.n, sizeof(word));
    *(r2.ptr) = vg2;
    r3.n = 10;
    r4 = *(r2.ptr);
    r5.n = 0;
    r6.ptr = r4.ptr + r5.n;
    r7 = *(r6.ptr);
    param[next_param++] = r2;
    param[next_param++] = r3;
    (*(r7.f))();
    r8 = r0;
    printf("%d\n", r8);
    return;
}

void DWC_Start() {
    word vl[2] = {0,0};
    word r9 = {0};
    word r8 = {0};
    word r7 = {0};
    word r6 = {0};
    word r5 = {0};
    word r4 = {0};
    word r3 = {0};
    word r2 = {0};
    word r1 = {0};
    int p;
    for(p = 0; p <= 1 && p < 4; p++) {
        vl[p] = param[p];
    }
    next_param = 0;
DWC_Start:
    r1 = *(vl[0].ptr);
    r2.n = 2;
    r3.ptr = r1.ptr + r2.n;
    r4 = *(r3.ptr);
    param[next_param++] = vl[0];
    param[next_param++] = vl[1];
    (*(r4.f))();
    r6 = *(vl[0].ptr);
    r7.n = 1;
    r8.ptr = r6.ptr + r7.n;
    r9 = *(r8.ptr);
    param[next_param++] = vl[0];
    param[next_param++] = vl[1];
    (*(r9.f))();
    r0.n = 0;
    return;
}

void DWC_Execute() {
    word vl[5] = {0,0,0,0,0};
    word r26 = {0};
    word r25 = {0};
    word r24 = {0};
    word r23 = {0};
    word r22 = {0};
    word r21 = {0};
    word r20 = {0};
    word r19 = {0};
    word r18 = {0};
    word r17 = {0};
    word r16 = {0};
    word r15 = {0};
    word r14 = {0};
    word r13 = {0};
    word r12 = {0};
    word r11 = {0};
    word r10 = {0};
    word r9 = {0};
    word r8 = {0};
    word r7 = {0};
    word r6 = {0};
    word r5 = {0};
    word r4 = {0};
    word r3 = {0};
    word r2 = {0};
    word r1 = {0};
    int p;
    for(p = 0; p <= 4 && p < 4; p++) {
        vl[p] = param[p];
    }
    next_param = 0;
DWC_Execute:
    vl[2].n = 0;
DWC_Execute_0:
    r5.n = 1234;
    printf("%d\n", r5);
    printf("%d\n", vl[2]);
    r6.n = 1;
    r7.ptr = vl[0].ptr + r6.n;
    r8 = *(r7.ptr);
    r9.n = 1;
    r10.ptr = r8.ptr + r9.n;
    r11.ptr = r10.ptr + vl[2].n;
    r12 = *(r11.ptr);
    vl[3] = r12;
    r13.n = 1;
    r14.ptr = vl[0].ptr + r13.n;
    r15 = *(r14.ptr);
    r16.n = 1;
    r17.n = vl[2].n + r16.n;
    r18.n = 1;
    r19.ptr = r15.ptr + r18.n;
    r20.ptr = r19.ptr + r17.n;
    r21 = *(r20.ptr);
    vl[4] = r21;
    printf("%d\n", vl[3]);
    printf("%d\n", vl[4]);
    r22.n = vl[3].n <= vl[4].n;
    if (r22.n == 0) goto DWC_Execute_1;
    r23.n = 1;
    printf("%d\n", r23);
    goto DWC_Execute_2;
DWC_Execute_1:
    r24.n = 0;
    printf("%d\n", r24);
DWC_Execute_2:
    r25.n = 1;
    r26.n = vl[2].n + r25.n;
    vl[2] = r26;
    r2.n = 1;
    r3.n = vl[1].n - r2.n;
    r4.n = vl[2].n <= r3.n;
    if (r4.n == 0) goto DWC_Execute_4;
    goto DWC_Execute_0;
DWC_Execute_4:
    r0.n = 0;
    return;
}

void DWC_Init() {
    word vl[2] = {0,0};
    word r85 = {0};
    word r84 = {0};
    word r83 = {0};
    word r82 = {0};
    word r81 = {0};
    word r80 = {0};
    word r79 = {0};
    word r78 = {0};
    word r77 = {0};
    word r76 = {0};
    word r75 = {0};
    word r74 = {0};
    word r73 = {0};
    word r72 = {0};
    word r71 = {0};
    word r70 = {0};
    word r69 = {0};
    word r68 = {0};
    word r67 = {0};
    word r66 = {0};
    word r65 = {0};
    word r64 = {0};
    word r63 = {0};
    word r62 = {0};
    word r61 = {0};
    word r60 = {0};
    word r59 = {0};
    word r58 = {0};
    word r57 = {0};
    word r56 = {0};
    word r55 = {0};
    word r54 = {0};
    word r53 = {0};
    word r52 = {0};
    word r51 = {0};
    word r50 = {0};
    word r49 = {0};
    word r48 = {0};
    word r47 = {0};
    word r46 = {0};
    word r45 = {0};
    word r44 = {0};
    word r43 = {0};
    word r42 = {0};
    word r41 = {0};
    word r40 = {0};
    word r39 = {0};
    word r38 = {0};
    word r37 = {0};
    word r36 = {0};
    word r35 = {0};
    word r34 = {0};
    word r33 = {0};
    word r32 = {0};
    word r31 = {0};
    word r30 = {0};
    word r29 = {0};
    word r28 = {0};
    word r27 = {0};
    word r26 = {0};
    word r25 = {0};
    word r24 = {0};
    word r23 = {0};
    word r22 = {0};
    word r21 = {0};
    word r20 = {0};
    word r19 = {0};
    word r18 = {0};
    word r17 = {0};
    word r16 = {0};
    word r15 = {0};
    word r14 = {0};
    word r13 = {0};
    word r12 = {0};
    word r11 = {0};
    word r10 = {0};
    word r9 = {0};
    word r8 = {0};
    word r7 = {0};
    word r6 = {0};
    word r5 = {0};
    word r4 = {0};
    word r3 = {0};
    word r2 = {0};
    word r1 = {0};
    int p;
    for(p = 0; p <= 1 && p < 4; p++) {
        vl[p] = param[p];
    }
    next_param = 0;
DWC_Init:
    r2.n = 1;
    r1.n = vl[1].n + r2.n;
    r3.ptr = calloc(r1.n, sizeof(word));
    *(r3.ptr) = vl[1];
    r5.n = 1;
    r4.ptr = vl[0].ptr + r5.n;
    *(r4.ptr) = r3;
    r6.n = 1;
    r7.ptr = vl[0].ptr + r6.n;
    r8 = *(r7.ptr);
    r9.n = 0;
    r10.n = 20;
    r11.n = 1;
    r12.ptr = r8.ptr + r11.n;
    r13.ptr = r12.ptr + r9.n;
    *(r13.ptr) = r10;
    r14.n = 1;
    r15.ptr = vl[0].ptr + r14.n;
    r16 = *(r15.ptr);
    r17.n = 1;
    r18.n = 7;
    r19.n = 1;
    r20.ptr = r16.ptr + r19.n;
    r21.ptr = r20.ptr + r17.n;
    *(r21.ptr) = r18;
    r22.n = 1;
    r23.ptr = vl[0].ptr + r22.n;
    r24 = *(r23.ptr);
    r25.n = 2;
    r26.n = 12;
    r27.n = 1;
    r28.ptr = r24.ptr + r27.n;
    r29.ptr = r28.ptr + r25.n;
    *(r29.ptr) = r26;
    r30.n = 1;
    r31.ptr = vl[0].ptr + r30.n;
    r32 = *(r31.ptr);
    r33.n = 3;
    r34.n = 18;
    r35.n = 1;
    r36.ptr = r32.ptr + r35.n;
    r37.ptr = r36.ptr + r33.n;
    *(r37.ptr) = r34;
    r38.n = 1;
    r39.ptr = vl[0].ptr + r38.n;
    r40 = *(r39.ptr);
    r41.n = 4;
    r42.n = 2;
    r43.n = 1;
    r44.ptr = r40.ptr + r43.n;
    r45.ptr = r44.ptr + r41.n;
    *(r45.ptr) = r42;
    r46.n = 1;
    r47.ptr = vl[0].ptr + r46.n;
    r48 = *(r47.ptr);
    r49.n = 5;
    r50.n = 11;
    r51.n = 1;
    r52.ptr = r48.ptr + r51.n;
    r53.ptr = r52.ptr + r49.n;
    *(r53.ptr) = r50;
    r54.n = 1;
    r55.ptr = vl[0].ptr + r54.n;
    r56 = *(r55.ptr);
    r57.n = 6;
    r58.n = 9;
    r59.n = 1;
    r60.ptr = r56.ptr + r59.n;
    r61.ptr = r60.ptr + r57.n;
    *(r61.ptr) = r58;
    r62.n = 1;
    r63.ptr = vl[0].ptr + r62.n;
    r64 = *(r63.ptr);
    r65.n = 7;
    r66.n = 9;
    r67.n = 1;
    r68.ptr = r64.ptr + r67.n;
    r69.ptr = r68.ptr + r65.n;
    *(r69.ptr) = r66;
    r70.n = 1;
    r71.ptr = vl[0].ptr + r70.n;
    r72 = *(r71.ptr);
    r73.n = 8;
    r74.n = 19;
    r75.n = 1;
    r76.ptr = r72.ptr + r75.n;
    r77.ptr = r76.ptr + r73.n;
    *(r77.ptr) = r74;
    r78.n = 1;
    r79.ptr = vl[0].ptr + r78.n;
    r80 = *(r79.ptr);
    r81.n = 9;
    r82.n = 5;
    r83.n = 1;
    r84.ptr = r80.ptr + r83.n;
    r85.ptr = r84.ptr + r81.n;
    *(r85.ptr) = r82;
    r0.n = 0;
    return;
}

