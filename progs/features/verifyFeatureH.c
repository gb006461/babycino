#include <stdio.h>
#include <stdbool.h>

int main() {
	int result;
	int count;
	bool done;
	
	result = 0;
	count = 1;
	done = true;
	
	do {
		result = result + count;
		count = count + 1;
		done = (10 < count);
	} while (!done);
		
	printf("%d\n", result);
		
	return 0;
}