echo Running the build script.
call build.bat
echo Compiling DoWhileComparison exercise program with babycino.
call babycino.bat progs\appel\DoWhileComparison.java doWhileComparison.c
echo Compiling resulting source with tcc.
call tcc.bat -o doWhileComparison.exe doWhileComparison.c
echo Running doWhileComparison program.
doWhileComparison.exe

