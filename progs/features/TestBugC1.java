class Main {
    public static void main(String[] args){
		System.out.println(new TestBugC1().Start());
	}
}


class TestBugC1 {
	public int Start() {
		boolean a;
		boolean b;
		
		a = false;
		b = true;
	
		if (a <= b) {
			System.out.println(1);
		} else {
			System.out.println(0);
		}
		
		return 0;
	}
}
