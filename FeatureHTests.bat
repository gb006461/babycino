echo off

echo Running the build script. > FeatureHTest_Output.txt
call build.bat silent >> FeatureHTest_Output.txt

echo . >> FeatureHTest_Output.txt
echo Test one: Compiler does not allow an integer as the condition. >> FeatureHTest_Output.txt
echo ---- Expected Result: Compilation failure >> FeatureHTest_Output.txt
echo ---- Actual Result: >> FeatureHTest_Output.txt

echo on
call babycino.bat progs\features\TestBugH1.java progs\features\testBugH.c >> FeatureHTest_Output.txt
echo off

echo . >> FeatureHTest_Output.txt
echo Compiling remaining Feature H tests with babycino. >> FeatureHTest_Output.txt
call babycino.bat progs\features\TestBugH2.java progs\features\testBugCH.c >> FeatureHTest_Output.txt
call babycino.bat progs\features\TestBugH2.java progs\features\testBugCH.c >> FeatureHTest_Output.txt

echo . >> FeatureHTest_Output.txt
echo Compiling resulting source code files with tcc. >> FeatureHTest_Output.txt
call tcc.bat -o progs\features\testBugH2.exe progs\features\testBugH2.c >> FeatureHTest_Output.txt
call tcc.bat -o progs\features\testBugH3.exe progs\features\testBugH3.c >> FeatureHTest_Output.txt

echo . >> FeatureHTest_Output.txt
echo Running tests... >> FeatureHTest_Output.txt

echo . >> FeatureHTest_Output.txt
echo Test two: Condition is checked at the end, not the start >> FeatureHTest_Output.txt
echo ---- Expected Result: Program should run the loop once, printing 1 to the console >> FeatureHTest_Output.txt
echo ---- Actual Result: >> FeatureHTest_Output.txt
progs\features\testBugH2.exe >> FeatureHTest_Output.txt

echo . >> FeatureHTest_Output.txt
echo Test three: Loop continues while condition is true >> FeatureHTest_Output.txt
echo ---- Expected Result: Loop should count up to less than 10, then end >> FeatureHTest_Output.txt
echo ----  Actual Result: >> FeatureHTest_Output.txt
progs\features\testBugH3.exe >> FeatureHTest_Output.txt

